#Node.js Chat app

Node.js simple chat app for multiple users with own location sharing.

###Technologies used:
- Node.js
- Socket.io

###DESCRIPTION:

- loging with username and room
- messages for entering and leaving user
- list of all users in room
- automatic scrolling for new messages
- button for sharing location