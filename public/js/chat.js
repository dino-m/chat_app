const socket = io()

// Elements
const $msgFrm = document.querySelector('#mainForm')
const $msgFrmInp = $msgFrm.querySelector('input')
const $msgFrmBtn = $msgFrm.querySelector('button')
const $locationBtn = document.querySelector('#send-location')
const $msgs = document.querySelector('#msgs')

// Templates
const $msgTempl = document.querySelector('#msg-templ').innerHTML
const $locTempl = document.querySelector('#loc-templ').innerHTML
const $sidebarTempl = document.querySelector('#sidebar-template').innerHTML

// Options
const {username, room} = Qs.parse(location.search, {ignoreQueryPrefix: true})

const autoscroll = () => {
    // New message element
    const $newMsg = $msgs.lastElementChild

    // Height of the new msg
    const newMsgStyles = getComputedStyle($newMsg)
    const newMsgMargin = parseInt(newMsgStyles.marginBottom)
    const newMsgHeight = $newMsg.offsetHeight + newMsgMargin

    // Visible height
    const visibleHeight = $msgs.offsetHeight

    // Height of msgs container
    const containerHeight = $msgs.scrollHeight

    // How far have I scrolled
    const scrollOffset = $msgs.scrollTop + visibleHeight

    if(containerHeight - newMsgHeight <= scrollOffset){
        $msgs.scrollTop = $msgs.scrollHeight
    }
}

socket.on('message', (msg) => {
    console.log(msg)
    const html = Mustache.render($msgTempl, {
        username: msg.username,
        msg: msg.text,
        createdAt: moment(msg.createdAt).format('h:mm a')
    })
    $msgs.insertAdjacentHTML('beforeend', html)
    autoscroll()
})

socket.on('locationMsg', (url) => {
    console.log(url)
    const html = Mustache.render($locTempl, {
        username: url.username,
        url: url.url,
        createdAt: moment(url.createdAt).format('h:mm a')
    })

    $msgs.insertAdjacentHTML('beforeend', html)
    autoscroll()
})

socket.on('roomData', ({room, users}) => {
    const html = Mustache.render($sidebarTempl, {
        room,
        users
    })

    document.querySelector('#sidebar').innerHTML = html
})

$msgFrm.addEventListener('submit', (e) => {
    e.preventDefault()

    $msgFrmBtn.setAttribute('disabled', 'disabled')

    const inputMsg = document.getElementById('inputMsg').value

    socket.emit('sendMessage', inputMsg, (error) => {
        $msgFrmBtn.removeAttribute('disabled')
        $msgFrmInp.value = ''
        $msgFrmInp.focus()

        if(error){
            return console.log(error)
        }

        console.log('Message delivered!')
    })

    //socket.emit('weatherMsg', inputMsg)
})

$locationBtn.addEventListener('click', () => {
    if(!navigator.geolocation){
        return alert('Geolocation is not supported by your browser!')
    }

    $locationBtn.setAttribute('disabled', 'disabled')

    navigator.geolocation.getCurrentPosition((position) => {
        console.log(position)

        socket.emit('sendLocation', {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        }, () => {
            $locationBtn.removeAttribute('disabled')
            console.log('Location shared!')
        })
    })
})

socket.emit('join', {username, room}, (error) => {
    if(error){
        alert(error)
        location.href = '/'
    }
})